<?php
// 本类由系统自动生成，仅供测试用途
class JiaocaiAction extends Action {

	
	//预设  para一般自身的所有以及扩展的zabojingua东西
	//聚合
    private $all=array(
    	'mdmk'=>'Jiaocai',//NB
  		'ttl'=>'教材',
  		'jn'=>array('tb_bd ON f_jiaocai_bdid=bdid'),//NB
      //自己的全部+f的显示的东西
  		'para'=>array('jiaocaiid'=>'ID','f_jiaocai_bdid'=>'板块','bdnm'=>'板块名称','jiaocaitpc'=>'标题','jiaocaiaddtm'=>'添加时间','jiaocaimdftm'=>'修改时间','jiaocaidownloadcnt'=>'下载次数','jiaocaizn'=>'赞','jiaocaitc'=>'吐槽','jiaocairsc'=>'文件资源','jiaocaismr'=>'摘要评论','jiaocaivideo'=>'视频','jiaocaivideocnt'=>'视频点播次数','jiaocaishunxu'=>'教材顺序'),//NB
  		//抛去不是zabojin的属性针对para
      'notself'=>array('bdnm'),
       ##########modify 添加修改中不需要展示和理会的属性 针对para
      'no_update'=>array('jiaocaiid','bdnm','jiaocaiaddtm','stdnm','f_std_sexid','sexnm','f_std_qiid','qinm','qiinttm'),
      #####update的时候允许为空的值 针对zabojin刨掉不然显示的update字段后
      'allowempty'=>array(),

      'hide_fld'=>array('jiaocaiid','f_jiaocai_bdid','jiaocairsc','jiaocaismr'),//NB
      'hide_cdt'=>array('jiaocaiid','bdnm','jiaocairsc'),//NB
  		
    //   'spccdtls'=>array('spccdt_0'=>array('aaid<>0','aaID不为0【废话只是测试】')),
  		// 'odrls'=>array('aanm'),
    //   'spccdt_dflt'=>array('spccdt_0'),
    //   'odr_dflt'=>array('aanm'=>'ASC'),

      'spccdtls'=>array(),//NB
      'odrls'=>array('jiaocaishunxu'),//NB
      'spccdt_dflt'=>array(),//NB
      'odr_dflt'=>array('jiaocaishunxu'=>'ASC'),//NB
      //hide的fld必须有，他们虽然不显示但是必须选择，这样才能在第一次进入query的时候，隐藏属性可以被调用，特别是id和fid
  		'fld_dflt'=>array('jiaocaiid','f_jiaocai_bdid','bdnm','jiaocaitpc','jiaocaiaddtm','jiaocaimdftm','jiaocaidownloadcnt','jiaocaizn','jiaocaitc','jiaocaivideocnt','jiaocaishunxu'),//NB
  		'cdt_dflt'=>array(),//NB
  		
  		'lmt_dflt'=>20,//NB
  		
  		'defaultls'=>1,//默认枚举//NB
  		##########view
  		'no_view'=>array('jiaocaiid','f_jiaocai_bdid','jiaocaivideo','jiaocairsc','jiaocaismr'),
	   
      #########删除提醒
      'deleteconfirm'=>'确定要删除此条记录？',
      #####转义
      'transmean'=>array(),//NB
      #####默认值
      'dfltvalue'=>array('jiaocaidownloadcnt'=>0,'jiaocaizn'=>0,'jiaocaitc'=>0,'jiaocaivideocnt'=>0),
      
    	);

    //公版
    public function query(){
    	header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');$tree=D('Tree');$bd=D('Bd');
    	$pb->query($this->all);
      #dingzhis
      #手动枚举并覆盖
      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $arr=$tree->unlimitedForListSLCT($bdls,0,'bdid','bdnm','bdpid','bdodr');
      $this->assign('f_jiaocai_bdid',$arr);
      #dingzhio
      $this->display('Cmn:query');
  
    }
    
    //dingzhi
    public function view(){
    	header("Content-Type:text/html; charset=utf-8");
    	//dingzhis
      $environment=D('Environment');$jiaocai=D('Jiaocai');$tree=D('Tree');$bd=D('Bd');

      $all=$this->all;

      $para=$all['para'];$this->assign('para',$para);
      $no_view=$all['no_view'];$this->assign('no_view',$no_view);

      $mdmk=$all['mdmk'];

      $arr_usross=$environment->setenvironment(MODULE_NAME);$usross=$arr_usross['data'];
      
      $jiaocaiid=$_GET['id'];
      $arr_mo=$jiaocai->getmo($jiaocaiid);$mo=$arr_mo['data'];

      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $str=$tree->findF($bdls, $mo['f_jiaocai_bdid'], 'bdid','bdnm','bdpid');
      $this->assign('tree',$str);

      

          
      $transmean=$all['transmean'];
      foreach($mo as $k=>$v){
        if(isset($transmean[$k])){
          $mo[$k]=$transmean[$k][$mo[$k]];
        }
      }
      $this->assign('mo',$mo);
      $this->assign('ttl',$mo['jiaocaitpc']);

      //dingzhio
		  $this->display('view');
    }
   
   	//
   	public function update(){
   		header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');$bd=D('Bd');$tree=D('Tree');
      $all=$this->all;
      $all['dfltvalue']['jiaocaimdftm']=date('Y-m-d H:i:s',time());
      $pb->update($all);
      //dingzhis
      $this->assign('project',C('PROJECT'));//为editor而生//为默认图片而生
      #手动枚举并覆盖
      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $arr=$tree->unlimitedForListSLCT($bdls,0,'bdid','bdnm','bdpid','bdodr');
      $this->assign('f_jiaocai_bdid',$arr);
      //dingzhio
		  $this->display('update');
   	}

   	//公版
   	public function doupdate(){
   		  $jiaocai=D('Jiaocai');

        $all=$this->all;
        $get=$_POST;
        
        $id=$get['jiaocaiid'];
        unset($get['jiaocaiid']);
        unset($get['_URL_']);

        

        if($id==0){
            if($get['jiaocaimdftm']){$jiaocaiaddtm=$get['jiaocaimdftm'];}else{$jiaocaiaddtm=date('Y-m-d h:m:s',time());}//要么是""要么是手动输入值
            $get['jiaocaiaddtm']=$jiaocaiaddtm;
            $get['jiaocaimdftm']=$jiaocaiaddtm;
            $jiaocai->add($get);
            $pattern=0;
        }else{
            if($get['jiaocaimdftm']){$jiaocaimdftm=$get['jiaocaimdftm'];}else{$jiaocaimdftm=date('Y-m-d h:m:s',time());}//要么手动时间要么是不传了
            $get['jiaocaimdftm']=$jiaocaimdftm;
            $jiaocai->mdf($get,$id);
            $pattern=1;
        }
        
        $data['pattern']=$pattern;
        $this->ajaxReturn($data,'json');
   	}

   	//公版
   	public function dodelete(){
   		header("Content-Type:text/html; charset=utf-8");
   		$pb=D('PB');
   		$pb->dodelete($this->all);
  		
   		$this->ajaxReturn($data,'json');
   	}

}