<?php
// 本类由系统自动生成，仅供测试用途
class WorkAction extends Action {

	
	//预设  para一般自身的所有以及扩展的zabojingua东西
	//聚合
    private $all=array(
    	'mdmk'=>'Work',//NB
  		'ttl'=>'作品',
  		'jn'=>array('tb_bd ON f_work_bdid=bdid','tb_std ON f_work_stdid=stdid','tb_sex ON f_std_sexid=sexid','tb_qi ON f_std_qiid=qiid'),//NB
      //自己的全部+f的显示的东西
  		'para'=>array('workid'=>'ID','f_work_bdid'=>'板块','bdnm'=>'板块名称','worktpc'=>'标题','f_work_stdid'=>'学生','stdnm'=>'学生姓名','f_std_sexid'=>'性别','sexnm'=>'性别','f_std_qiid'=>'办学班次','qinm'=>'办学班次名称','qiinttm'=>'办学班次起始时间','workaddtm'=>'添加时间','workmdftm'=>'修改时间','workcnt'=>'阅读数','workzn'=>'赞','worktc'=>'吐槽','workrsc'=>'文件资源','worksmr'=>'摘要评论','workmark'=>'评分'),//NB
  		//抛去不是zabojin的属性针对para
      'notself'=>array('bdnm','stdnm','f_std_sexid','sexnm','f_std_qiid','qinm','qiinttm'),
       ##########modify 添加修改中不需要展示和理会的属性 针对para
      'no_update'=>array('workid','bdnm','workaddtm','stdnm','f_std_sexid','sexnm','f_std_qiid','qinm','qiinttm'),
      #####update的时候允许为空的值 针对zabojin刨掉不然显示的update字段后
      'allowempty'=>array(),

      'hide_fld'=>array('workid','f_work_bdid','f_std_sexid','f_std_qiid','workrsc','worksmr'),//NB
      'hide_cdt'=>array('workid','bdnm','stdnm','sexnm','qinm','qiinttm','workrsc'),//NB
  		
    //   'spccdtls'=>array('spccdt_0'=>array('aaid<>0','aaID不为0【废话只是测试】')),
  		// 'odrls'=>array('aanm'),
    //   'spccdt_dflt'=>array('spccdt_0'),
    //   'odr_dflt'=>array('aanm'=>'ASC'),

      'spccdtls'=>array(),//NB
      'odrls'=>array('workmdftm'),//NB
      'spccdt_dflt'=>array(),//NB
      'odr_dflt'=>array('workmdftm'=>'DESC'),//NB
      //hide的fld必须有，他们虽然不显示但是必须选择，这样才能在第一次进入query的时候，隐藏属性可以被调用，特别是id和fid
  		'fld_dflt'=>array('workid','f_work_bdid','bdnm','worktpc','f_work_stdid','stdnm','f_std_sexid','sexnm','f_std_qiid','qinm','workaddtm','workmdftm','workcnt','workzn','worktc','workmark'),//NB
  		'cdt_dflt'=>array(),//NB
  		
  		'lmt_dflt'=>20,//NB
  		
  		'defaultls'=>1,//默认枚举//NB
  		##########view
  		'no_view'=>array('workid','f_work_bdid','f_std_sexid','f_std_qiid','workrsc','worksmr'),
	   
      #########删除提醒
      'deleteconfirm'=>'确定要删除此条记录？',
      #####转义
      'transmean'=>array(),//NB
      #####默认值
      'dfltvalue'=>array('workcnt'=>0,'workzn'=>0,'worktc'=>0),
      
    	);

    //公版
    public function query(){
    	header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');$tree=D('Tree');$bd=D('Bd');
    	$pb->query($this->all);
      #dingzhis
      #手动枚举并覆盖
      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $arr=$tree->unlimitedForListSLCT($bdls,0,'bdid','bdnm','bdpid','bdodr');
      $this->assign('f_work_bdid',$arr);
      #dingzhio
      $this->display('Cmn:query');
  
    }
    
    //dingzhi
    public function view(){
    	header("Content-Type:text/html; charset=utf-8");
    	//dingzhis
      $environment=D('Environment');$work=D('Work');$tree=D('Tree');$bd=D('Bd');

      $all=$this->all;

      $para=$all['para'];$this->assign('para',$para);
      $no_view=$all['no_view'];$this->assign('no_view',$no_view);

      $mdmk=$all['mdmk'];

      $arr_usross=$environment->setenvironment(MODULE_NAME);$usross=$arr_usross['data'];
      
      $workid=$_GET['id'];
      $arr_mo=$work->getmo($workid);$mo=$arr_mo['data'];

      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $str=$tree->findF($bdls, $mo['f_work_bdid'], 'bdid','bdnm','bdpid');
      $this->assign('tree',$str);

      

      $arr_nwcnt=$work->addworkcnt($mo['workcnt'],$workid);$nwcnt=$arr_nwcnt['data'];
      $mo['workcnt']=$nwcnt;

     
      $transmean=$all['transmean'];
      foreach($mo as $k=>$v){
        if(isset($transmean[$k])){
          $mo[$k]=$transmean[$k][$mo[$k]];
        }
      }
      $this->assign('mo',$mo);
      $this->assign('ttl',$mo['worktpc']);

      //dingzhio
		  $this->display('view');
    }
   
   	//
   	public function update(){
   		header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');$bd=D('Bd');$tree=D('Tree');
      $all=$this->all;
      $all['dfltvalue']['workmdftm']=date('Y-m-d H:i:s',time());
      $pb->update($all);
      //dingzhis
      $this->assign('project',C('PROJECT'));//为editor而生//为默认图片而生
      #手动枚举并覆盖
      $arr_bdls=$bd->getmlsbyodr('bdodr ASC');$bdls=$arr_bdls['data'];
      $arr=$tree->unlimitedForListSLCT($bdls,0,'bdid','bdnm','bdpid','bdodr');
      $this->assign('f_work_bdid',$arr);
      //dingzhio
		  $this->display('update');
   	}

   	//公版
   	public function doupdate(){
   		  $work=D('Work');

        $all=$this->all;
        $get=$_POST;
        
        $id=$get['workid'];
        unset($get['workid']);
        unset($get['_URL_']);

        

        if($id==0){
            if($get['workmdftm']){$workaddtm=$get['workmdftm'];}else{$workaddtm=date('Y-m-d h:m:s',time());}//要么是""要么是手动输入值
            $get['workaddtm']=$workaddtm;
            $get['workmdftm']=$workaddtm;
            $work->add($get);
            $pattern=0;
        }else{
            if($get['workmdftm']){$workmdftm=$get['workmdftm'];}else{$workmdftm=date('Y-m-d h:m:s',time());}//要么手动时间要么是不传了
            $get['workmdftm']=$workmdftm;
            $work->mdf($get,$id);
            $pattern=1;
        }
        
        $data['pattern']=$pattern;
        $this->ajaxReturn($data,'json');
   	}

   	//公版
   	public function dodelete(){
   		header("Content-Type:text/html; charset=utf-8");
   		$pb=D('PB');
   		$pb->dodelete($this->all);
  		
   		$this->ajaxReturn($data,'json');
   	}

}