<?php
// 本类由系统自动生成，仅供测试用途
class StdAction extends Action {

	
	//预设  para一般自身的所有以及扩展的zabojingua东西
	//聚合
    private $all=array(
    	'mdmk'=>'Std',//NB
  		'ttl'=>'学生',
  		'jn'=>array('tb_sex ON f_std_sexid=sexid','tb_qi ON f_std_qiid=qiid'),//NB
      //自己的全部+f的显示的东西
  		'para'=>array('stdid'=>'stdID','stdnm'=>'姓名','stdpw'=>'密码','f_std_sexid'=>'性别','sexnm'=>'性别','stdgrd'=>'年级','f_std_qiid'=>'办学班次','qinm'=>'办学班次名称','qiinttm'=>'办学班次起始时间'),//NB
  		//抛去不是zabojin的属性针对para
      'notself'=>array('sexnm','qinm','qiinttm'),
       ##########modify 添加修改中不需要展示和理会的属性 针对para
      'no_update'=>array('stdid','sexnm','qinm','qiinttm'),
      #####update的时候允许为空的值 针对zabojin刨掉不然显示的update字段后
      'allowempty'=>array('stdpw','stdgrd'),

      'hide_fld'=>array('stdid','f_std_sexid','f_std_qiid'),//NB
      'hide_cdt'=>array('stdid','sexnm','qinm','qiinttm'),//NB
  		
    //   'spccdtls'=>array('spccdt_0'=>array('stdid<>0','stdID不为0【废话只是测试】')),
  		// 'odrls'=>array('stdnm'),
    //   'spccdt_dflt'=>array('spccdt_0'),
    //   'odr_dflt'=>array('stdnm'=>'ASC'),

      'spccdtls'=>array(),//NB
      'odrls'=>array(),//NB
      'spccdt_dflt'=>array(),//NB
      'odr_dflt'=>array(),//NB
      //hide的fld必须有，他们虽然不显示但是必须选择，这样才能在第一次进入query的时候，隐藏属性可以被调用，特别是id和fid
  		'fld_dflt'=>array('stdid','stdnm','stdpw','f_std_sexid','sexnm','stdgrd','f_std_qiid','qinm','qiinttm'),//NB
  		'cdt_dflt'=>array(),//NB
  		
  		'lmt_dflt'=>20,//NB
  		
  		'defaultls'=>1,//默认枚举//NB
  		##########view
  		'no_view'=>array('stdid','f_std_sexid','f_std_qiid'),
	   
      #########删除提醒
      'deleteconfirm'=>'确定要删除此条记录？',
      #####转义
      'transmean'=>array(),//NB
      #####默认值
      'dfltvalue'=>array(),
      
    	);

    //公版
    public function query(){
    	header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');
    	$pb->query($this->all);
      $this->display('Cmn:query');
  
    }
    
    //公版
    public function view(){
    	header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');
    	$pb->view($this->all);
		  $this->display('Cmn:view');
    }
   
   	//公版
   	public function update(){
   		header("Content-Type:text/html; charset=utf-8");
    	$pb=D('PB');
    	$pb->update($this->all);
		  $this->display('Cmn:update');
   	}

   	//公版
   	public function doupdate(){
   		header("Content-Type:text/html; charset=utf-8");
   		$std=D('Std');
   		$arr_pattern=$std->doupdate($this->all);
   		$data['pattern']=$arr_pattern['data'];

   		$this->ajaxReturn($data,'json');
   	}

   	//公版
   	public function dodelete(){
   		header("Content-Type:text/html; charset=utf-8");
   		$pb=D('PB');
   		$pb->dodelete($this->all);
  		
   		$this->ajaxReturn($data,'json');
   	}

}