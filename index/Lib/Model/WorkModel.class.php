<?php
class WorkModel extends Action{
	
	//公版
	public function getmo($workid){
		$info=collectinfo(__METHOD__,'$workid',array($workid));
		if(isset($workid)===false){return createarrerr('error_code','workid 不能为空',$info);}//防止NULL
		
		$work=M('work');
		$worko=$work->join('tb_bd ON f_work_bdid=bdid')->join('tb_std ON f_work_stdid=stdid')->join('tb_sex ON f_std_sexid=sexid')->join('tb_qi ON f_std_qiid=qiid')->where('workid='.$workid)->find();

		
		return createarrok('ok',$worko,'',$info);
	}

	//公版
	public function getworklsbystdid($stdid){
		$info=collectinfo(__METHOD__,'$stdid',array($stdid));
		if(isset($stdid)===false){return createarrerr('error_code','stdid 不能为空',$info);}//防止NULL
		
		$work=M('work');
		$workls=$work->join('tb_bd ON f_work_bdid=bdid')->join('tb_std ON f_work_stdid=stdid')->join('tb_sex ON f_std_sexid=sexid')->join('tb_qi ON f_std_qiid=qiid')->where('f_work_stdid='.$stdid)->order('workmdftm DESC')->select();

		
		return createarrok('ok',$workls,'',$info);
	}

	// //公版
	// public function getmls(){
	// 	$info=collectinfo(__METHOD__,'',array());
	// 	$work=M('work');
	// 	$workls=$work->join('tb_bd ON f_work_bdid=bdid')->select();
	// 	return createarrok('ok',$workls,'',$info);
	// }

	// //公版
	// public function getmlsbybdid($bdid){
	// 	$info=collectinfo(__METHOD__,'$bdid',array($bdid));
	// 	if(isset($bdid)===false){return createarrerr('error_code','bdid 不能为空',$info);}//防止NULL

	// 	$work=M('work');
	// 	$workls=$work->join('tb_bd ON f_work_bdid=bdid')->where('f_work_bdid='.$bdid)->select();

	// 	return createarrok('ok',$workls,'',$info);
	// }
	
	// //公版
	// public function deletebybdid($bdid){
	// 	$info=collectinfo(__METHOD__,'$bdid',array($bdid));
	// 	if(isset($bdid)===false){return createarrerr('error_code','bdid 不能为空',$info);}//防止NULL
		
	// 	$arr_workls=$this->getmlsbybdid($bdid);$workls=$arr_workls['data'];
	// 	foreach($workls as $workv){
	// 		$this->delete($workv['workid']);
	// 	}

	// 	return createarrok('ok',$data,'',$info);
	// }

	// //公版
	// public function delete($workid){
	// 	$info=collectinfo(__METHOD__,'$workid',array($workid));
	// 	if(isset($workid)===false){return createarrerr('error_code','workid 不能为空',$info);}//防止NULL
		
	// 	$work=M('work');$bdwork=D('Ccwork');
	// 	$work->where('workid='.$workid)->delete();
	// 	//删除依赖
 //      	$bdwork->deletebyworkid($workid);
      	
	// 	return createarrok('ok',$data,'',$info);
	// }

	//公版
	public function add($get){
		$info=collectinfo(__METHOD__,'$get',array($get));
		if(isset($get)===false){return createarrerr('error_code','get 不能为空',$info);}//防止NULL
		
		$work=M('work');
		$work->data($get)->add();

		return createarrok('ok',$data,'',$info);
	}

	//公版
	public function mdf($get,$id){
		$info=collectinfo(__METHOD__,'$get,$id',array($get,$id));
		if(isset($get)===false){return createarrerr('error_code','get 不能为空',$info);}//防止NULL
		if(isset($id)===false){return createarrerr('error_code','id 不能为空',$info);}//防止NULL

		$work=M('work');
		$work->where('workid='.$id)->setField($get);

		return createarrok('ok',$data,'',$info);
	}

	//
	public function addworkcnt($origincnt,$workid){
		$info=collectinfo(__METHOD__,'$origincnt,$workid',array($origincnt,$workid));
		if(isset($origincnt)===false){return createarrerr('error_code','origincnt 不能为空',$info);}//防止NULL
		if(isset($workid)===false){return createarrerr('error_code','workid 不能为空',$info);}//防止NULL

		$work=M('work');
		$nwcnt=$origincnt+1;
		$dt=array('workcnt'=>$nwcnt);
		$work->where('workid='.$workid)->setField($dt);

		return createarrok('ok',$nwcnt,'',$info);
	}



} 
?>