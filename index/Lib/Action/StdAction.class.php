<?php
// 本类由系统自动生成，仅供测试用途
class StdAction extends Action {
	//预设  para一般自身的所有以及扩展的zabojingua东西
    //聚合
    private $all=array(
        'mdmk'=>'Std',
        'ttl'=>'学生',
        'jn'=>array(),
        'para'=>array('stdid'=>'stdID','stdmk'=>'是否管理员','stdnm'=>'用户名','stdpw'=>'密码','stdnn'=>'真名','stdpt'=>'头像','stdaddtm'=>'添加时间','stdmdftm'=>'修改时间','stdcp'=>'手机号','stdml'=>'邮箱','stdps'=>'是否通过','stdvw'=>'是否查看过','stdodr'=>'顺序'),
        //抛去不是zabojin的属性
        'notself'=>array(),
        'hide_fld'=>array('stdid','stdpw'),
        'hide_cdt'=>array('stdid','stdpw','stdpt','stdodr'),

        //   'spccdtls'=>array('spccdt_0'=>array('aaid<>0','aaID不为0【废话只是测试】')),
        // 'odrls'=>array('aanm'),
        //   'spccdt_dflt'=>array('spccdt_0'),
        //   'odr_dflt'=>array('aanm'=>'ASC'),

        'spccdtls'=>array(),
        'odrls'=>array('stdodr'),
        'spccdt_dflt'=>array(),
        'odr_dflt'=>array('stdodr'=>'ASC'),

        'fld_dflt'=>array('stdid','stdmk','stdnm','stdnn','stdpt','stdaddtm','stdmdftm','stdcp','stdml','stdps','stdvw','stdodr'),
        'cdt_dflt'=>array(),
        
        'lmt_dflt'=>10,
        
        'defaultls'=>1,//默认枚举
        ##########view
        'no_view'=>array('stdid','stdpw'),
        ##########modify//不用显示不用考虑的属性
        'no_update'=>array('stdid','stdpw','stdaddtm','stdmdftm'),
        #########删除提醒
        'deleteconfirm'=>'删除用户会导致stdrl相应的数据删除，确定？',
        #####转义
        'transmean'=>array('stdmk'=>array('0'=>'否','1'=>'是'),'stdps'=>array('0'=>'否','1'=>'是'),'stdvw'=>array('0'=>'否','1'=>'是')),
        #####默认值
        'dfltvalue'=>array('stdmk'=>0,'stdpw'=>'11111111','stdpt'=>'/xxx/Public/img/std/default.jpg','stdps'=>1,'stdvw'=>1),
        #####update的时候允许为空的值抛去no_update不用管的哪些zabojin的属性外允许空的
        'allowempty'=>array('stdcp','stdml'),
        //dingzhi
        'no_stdct'=>array('stdid','stdpw','stdvw','stdodr'),
        'no_stdupdate'=>array('stdid','stdmk','stdpw','stdaddtm','stdmdftm','stdps','stdvw','stdodr'),
        );

    //定制
    public function query(){
        header("Content-Type:text/html; charset=utf-8");
        $pb=D('PB');
        $pb->query($this->all);
        $this->display('query');
  
    }
    
    //定制
    public function view(){
        header("Content-Type:text/html; charset=utf-8");
        $pb=D('PB');
        $pb->view($this->all);
        $this->display('view');
    }
   
    //定制
    public function update(){
        header("Content-Type:text/html; charset=utf-8");
        $pb=D('PB');
        $pb->update($this->all);
        $this->display('update');
    }

    //定制
    public function doupdate(){
        header("Content-Type:text/html; charset=utf-8");
        $std=D('Std');

        $all=$this->all;
        $get=$_GET;
        
        $id=$get['stdid'];
        unset($get['stdid']);
        unset($get['_URL_']);

        if($id==0){
            $get['stdpw']=md5('11111111');
            $get['stdaddtm']=date('Y-m-d h:m:s');
            $get['stdmdftm']=date('Y-m-d h:m:s');
            $std->add($get);
            $pattern=0;
        }else{
            $get['stdmdftm']=date('Y-m-d h:m:s');
            $std->mdf($get,$id);
            $pattern=1;
        }
        
        $data['pattern']=$pattern;
        $this->ajaxReturn($data,'json');
    }

    //dingzhi
    public function dodelete(){
      header("Content-Type:text/html; charset=utf-8");
      //dingzhis
      $std=D('Std');
      
      $stdid=$_GET['id'];
      $std->delete($stdid);
      //dingzhio
      $this->ajaxReturn($data,'json');
    }
    
	
	//#########index
    public function dologin(){
    	header("Content-Type:text/html; charset=utf-8");
		    
    	$std=D('Std');
		
    	$stdnm=$_GET['stdnm'];
    	$stdpw=$_GET['stdpw'];
    	$rmb=$_GET['rmb'];

    	$arr=$std->login($stdnm,$stdpw,$rmb);
    	$rslt=$arr['data']['rslt'];
    	$msg=$arr['msg'];

    	$data['rslt']=$rslt;$data['msg']=$msg;
		$this->ajaxReturn($data,'json');
	}

    //#########index
    public function doresetstdpw(){
        header("Content-Type:text/html; charset=utf-8");
            
        $std=D('Std');

        $id=$_GET['id'];
        
        $arr=$std->resetstdpw($id);
        $msg=$arr['msg'];

        $data['msg']=$msg;
        $this->ajaxReturn($data,'json');
    }

    //定制
    public function stdct(){
        header("Content-Type:text/html; charset=utf-8");
        
		$environment=D('Environment');
    	
    	$all=$this->all;
    	$mdmk=$all['mdmk'];
    	$lowmdmk=strtolower($mdmk);$this->assign('lowmdmk',$lowmdmk);

    	$arr_stdoss=$environment->setenvironment(MODULE_NAME);$stdoss=$arr_stdoss['data'];
    	
    	$para=$all['para'];$this->assign('para',$para);
    	$no_stdct=$all['no_stdct'];$this->assign('no_stdct',$no_stdct);

    	$mo=$stdoss;

    	$transmean=$all['transmean'];
    	foreach($mo as $k=>$v){
    		if(isset($transmean[$k])){
    			$mo[$k]=$transmean[$k][$mo[$k]];
    		}
    	}
    	
    	$this->assign('mo',$mo);
    	$this->assign('ttl',$mo['stdnn']);
        $this->display('stdct');
    }

    //定制
    public function stdupdate(){
        header("Content-Type:text/html; charset=utf-8");

        $environment=D('Environment');
    	$all=$this->all;
    	$mdmk=$all['mdmk'];
    	$lowmdmk=strtolower($mdmk);$this->assign('lowmdmk',$lowmdmk);
    	$notself=$all['notself'];$this->assign('notself',$notself);
    	$transmean=$all['transmean'];$this->assign('transmean',$transmean);


    	$arr_stdoss=$environment->setenvironment(MODULE_NAME);$stdoss=$arr_stdoss['data'];
    	
    	$id=$stdoss['stdid'];$this->assign('id',$id);
    	$para=$all['para'];$this->assign('para',$para);
    	$jn=$all['jn'];
    	$no_stdupdate=$all['no_stdupdate'];$this->assign('no_stdupdate',$no_stdupdate);
    	$dfltvalue=$all['dfltvalue'];
    	$allowempty=$all['allowempty'];$this->assign('allowempty',$allowempty);

    	$defaultls=$all['defaultls'];
    	if($defaultls){
	    	//甭管添加还是修改 zabojingua 属性必须要ls给好
	    	foreach($para as $k=>$v){
	    		if(!in_array($k,$notself)){
					$tmp=explode('_', $k);
					$tmp=explode('id',$tmp[2]);
					$tmp=$tmp[0];$tmp=M($tmp);
					$this->assign($k,$tmp->select());
				}
				if(isset($transmean[$k])){
					$this->assign($k,$transmean[$k]);
				}
			}
		}

    	if($id==0){$mo=$dfltvalue;$pattern='注册';}else{$mo=$stdoss;$pattern='修改个人信息';}
    	
    	$this->assign('mo',$mo);
    	$this->assign('moforjs',transforjs($mo));
    	$this->assign('ttl',$pattern);
        $this->display('stdupdate');
    }

    public function stdmdfpw(){
    	header("Content-Type:text/html; charset=utf-8");

    	$environment=D('Environment');
    	$arr_stdoss=$environment->setenvironment(MODULE_NAME);$stdoss=$arr_stdoss['data'];
    	$id=$stdoss['stdid'];$this->assign('id',$id);
    	$this->assign('mo',$stdoss);

    	$this->assign('ttl','修改密码');
        $this->display('stdmdfpw');
    }

    public function domdfstdpw(){
    	header("Content-Type:text/html; charset=utf-8");
            
        $std=D('Std');

        $stdid=$_GET['stdid'];
        $originstdpw=$_GET['originstdpw'];
        $nwstdpw=$_GET['nwstdpw'];

        $arr_check=$std->checkstdpw($stdid,$originstdpw);
        $rslt=$arr_check['data'];
        if($rslt==1){
        	$std->mdfstdpw($stdid,$nwstdpw);
        }else{
        	$msg=$arr_check['msg'];
        }

        $data['rslt']=$rslt;
        $data['msg']=$msg;
       
        $this->ajaxReturn($data,'json');
    }

    public function dologinout(){
        header("Content-Type:text/html; charset=utf-8");
        
        session('stdidss',null);
        cookie('stdidck',null);
        
        $this->ajaxReturn($data,'json');
    }
}