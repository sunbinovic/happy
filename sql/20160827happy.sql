/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.1.8-MariaDB : Database - db_happy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_happy` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_happy`;

/*Table structure for table `tb_aa` */

DROP TABLE IF EXISTS `tb_aa`;

CREATE TABLE `tb_aa` (
  `aaid` int(2) NOT NULL AUTO_INCREMENT,
  `aanm` varchar(10) DEFAULT NULL,
  `f_aa_bbid` int(2) DEFAULT NULL,
  `aastat` int(1) DEFAULT NULL,
  PRIMARY KEY (`aaid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_aa` */

insert  into `tb_aa`(`aaid`,`aanm`,`f_aa_bbid`,`aastat`) values (1,'a一一',1,1),(2,'a二二',2,1),(3,'acc',2,0);

/*Table structure for table `tb_atc` */

DROP TABLE IF EXISTS `tb_atc`;

CREATE TABLE `tb_atc` (
  `atcid` int(3) NOT NULL AUTO_INCREMENT,
  `f_atc_bdid` int(5) DEFAULT NULL,
  `atctpc` varchar(50) DEFAULT NULL,
  `atcath` varchar(20) DEFAULT NULL,
  `atcaddtm` varchar(20) DEFAULT NULL,
  `atcmdftm` varchar(20) DEFAULT NULL,
  `atctp` tinyint(1) DEFAULT NULL,
  `atcps` tinyint(1) DEFAULT NULL,
  `atcanc` tinyint(1) DEFAULT NULL COMMENT '通知',
  `atcdnmc` tinyint(1) DEFAULT NULL COMMENT '动态',
  `atcctt` mediumtext,
  `atccnt` int(5) DEFAULT NULL,
  `atcnw` tinyint(1) DEFAULT NULL COMMENT '内网',
  `atczn` int(5) DEFAULT NULL COMMENT '赞',
  `atctc` int(5) DEFAULT NULL COMMENT '吐槽',
  `atcvw` tinyint(1) DEFAULT NULL,
  `atccv` varchar(100) DEFAULT NULL COMMENT '封面',
  `atcsmr` mediumtext COMMENT '摘要',
  PRIMARY KEY (`atcid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `tb_atc` */

/*Table structure for table `tb_ath` */

DROP TABLE IF EXISTS `tb_ath`;

CREATE TABLE `tb_ath` (
  `athid` int(10) NOT NULL AUTO_INCREMENT,
  `f_ath_rlid` int(10) DEFAULT NULL,
  `f_ath_mdid` int(10) DEFAULT NULL,
  `atha` tinyint(1) DEFAULT NULL,
  `athd` tinyint(1) DEFAULT NULL,
  `athm` tinyint(1) DEFAULT NULL,
  `athv` tinyint(1) DEFAULT NULL,
  `aths` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`athid`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

/*Data for the table `tb_ath` */

insert  into `tb_ath`(`athid`,`f_ath_rlid`,`f_ath_mdid`,`atha`,`athd`,`athm`,`athv`,`aths`) values (1,1,1,0,0,0,0,0),(2,1,2,0,0,0,0,0),(3,1,3,0,0,0,0,0),(4,1,4,0,0,0,0,0),(5,1,5,0,0,0,0,0),(6,1,6,0,0,0,0,0),(7,1,7,0,0,0,0,0),(8,1,9,0,0,0,0,0),(9,1,11,0,0,0,0,0),(10,1,14,0,0,0,0,0),(11,1,16,0,0,0,0,0),(12,1,17,0,0,0,0,0),(13,1,18,0,0,0,0,0),(14,1,19,0,0,0,0,0),(15,1,20,0,0,0,0,0),(16,1,21,0,0,0,0,0),(17,1,22,0,0,0,0,0),(18,1,23,0,0,0,0,0),(19,1,25,0,0,0,0,0),(20,0,1,0,0,0,0,0),(21,0,2,0,0,0,0,0),(22,0,3,0,0,0,0,0),(23,0,4,0,0,0,0,0),(24,0,5,0,0,0,0,0),(25,0,6,0,0,0,0,0),(26,0,7,0,0,0,0,0),(27,0,9,0,0,0,0,0),(28,0,11,0,0,0,0,0),(29,0,14,0,0,0,0,0),(30,0,16,0,0,0,0,0),(31,0,17,0,0,0,0,0),(32,0,18,0,0,0,0,0),(33,0,19,0,0,0,0,0),(34,0,20,0,0,0,0,0),(35,0,21,0,0,0,0,0),(36,0,22,0,0,0,0,0),(37,0,23,0,0,0,0,0),(38,0,25,0,0,0,0,0),(39,2,1,0,0,0,0,0),(40,2,2,0,0,0,0,0),(41,2,3,0,0,0,0,0),(42,2,4,0,0,0,0,0),(43,2,5,0,0,0,0,0),(44,2,6,0,0,0,0,0),(45,2,7,0,0,0,0,0),(46,2,9,0,0,0,0,0),(47,2,11,0,0,0,0,0),(48,2,14,0,0,0,0,0),(49,2,16,0,0,0,0,0),(50,2,17,0,0,0,0,0),(51,2,18,0,0,0,0,0),(52,2,19,0,0,0,0,0),(53,2,20,0,0,0,0,0),(54,2,21,0,0,0,0,0),(55,2,22,0,0,0,0,0),(56,2,23,0,0,0,0,0),(57,2,25,0,0,0,0,0),(58,1,0,0,0,0,0,0),(59,2,0,0,0,0,0,0),(60,1,0,0,0,0,0,0),(61,2,0,0,0,0,0,0),(62,1,0,0,0,0,0,0),(63,2,0,0,0,0,0,0),(64,1,0,0,0,0,0,0),(65,2,0,0,0,0,0,0);

/*Table structure for table `tb_bb` */

DROP TABLE IF EXISTS `tb_bb`;

CREATE TABLE `tb_bb` (
  `bbid` int(2) NOT NULL AUTO_INCREMENT,
  `bbnm` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`bbid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_bb` */

insert  into `tb_bb`(`bbid`,`bbnm`) values (1,'b一一'),(2,'b二二');

/*Table structure for table `tb_bd` */

DROP TABLE IF EXISTS `tb_bd`;

CREATE TABLE `tb_bd` (
  `bdid` int(10) NOT NULL AUTO_INCREMENT,
  `bdnm` varchar(20) DEFAULT NULL,
  `bdpid` int(10) DEFAULT NULL,
  `bdodr` int(10) DEFAULT NULL,
  PRIMARY KEY (`bdid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk;

/*Data for the table `tb_bd` */

insert  into `tb_bd`(`bdid`,`bdnm`,`bdpid`,`bdodr`) values (1,'Scratch',0,1);

/*Table structure for table `tb_grp` */

DROP TABLE IF EXISTS `tb_grp`;

CREATE TABLE `tb_grp` (
  `grpid` int(5) NOT NULL AUTO_INCREMENT,
  `grpnm` varchar(20) DEFAULT NULL,
  `grppid` int(5) DEFAULT NULL,
  `grpodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`grpid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_grp` */

insert  into `tb_grp`(`grpid`,`grpnm`,`grppid`,`grpodr`) values (1,'dflt',0,1),(2,'测试',0,2),(3,'测试-视频组',2,1);

/*Table structure for table `tb_jiaocai` */

DROP TABLE IF EXISTS `tb_jiaocai`;

CREATE TABLE `tb_jiaocai` (
  `jiaocaiid` int(5) NOT NULL AUTO_INCREMENT,
  `f_jiaocai_bdid` int(5) DEFAULT NULL,
  `jiaocaitpc` varchar(50) DEFAULT NULL,
  `jiaocaiaddtm` varchar(20) DEFAULT NULL,
  `jiaocaimdftm` varchar(20) DEFAULT NULL,
  `jiaocaidownloadcnt` int(5) DEFAULT NULL,
  `jiaocaizn` int(5) DEFAULT NULL COMMENT '赞',
  `jiaocaitc` int(5) DEFAULT NULL COMMENT '吐槽',
  `jiaocairsc` varchar(100) DEFAULT NULL,
  `jiaocaismr` mediumtext COMMENT '摘要',
  `jiaocaivideo` varchar(500) DEFAULT NULL,
  `jiaocaivideocnt` int(10) DEFAULT NULL,
  `jiaocaishunxu` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`jiaocaiid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=gbk;

/*Data for the table `tb_jiaocai` */

insert  into `tb_jiaocai`(`jiaocaiid`,`f_jiaocai_bdid`,`jiaocaitpc`,`jiaocaiaddtm`,`jiaocaimdftm`,`jiaocaidownloadcnt`,`jiaocaizn`,`jiaocaitc`,`jiaocairsc`,`jiaocaismr`,`jiaocaivideo`,`jiaocaivideocnt`,`jiaocaishunxu`) values (2,1,'正方形画花','2016-07-22 00:06:40','2016-07-22 00:06:41',0,0,0,'/happy/Uploads/workrsc/1469117230.sb2','在书本的基础上加上了自己的见解，很好','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MjE1Ng==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'2-1'),(3,1,'小猫追小球','2016-07-22 00:09:33','2016-07-22 00:09:34',0,0,0,'/happy/Uploads/workrsc/1469117383.sb2','学生很好理解了，猫，球，鼠标之间的内在联系，同时初步接触到延时的概念','小猫追小球 <embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MTI4NA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'2-2'),(4,1,'小猫抢钱袋','2016-07-22 11:00:28','2016-07-22 11:00:29',0,0,0,'/happy/Uploads/workrsc/1469156492.sb2','通过本次实验，孩子初步感受到了虚化的含义。对坐标轴中心点加深了理解，对面向的角度有了一定的认识，对复杂事件中类似的动作有了一定的提取能力，为日后软件分工，以及对未来分工的思维模式有了很好的养成，增加了变量的概念，同时在数学上对步长的理解进一步加深','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1Njk2NA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'2-3'),(5,1,'接苹果游戏','2016-07-22 11:01:58','2016-07-22 11:01:59',0,0,0,'/happy/Uploads/workrsc/1469156561.sb2','进一步加强对坐标轴的理解，首次接触到克隆物体，并对克隆物体进行操作','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MzUzNg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'2-4'),(6,1,'火箭打气球','2016-07-22 11:03:03','2016-07-22 11:03:05',2,0,0,'/happy/Uploads/workrsc/1469156617.sb2','进一步加强对坐标轴的理解，首次接触到克隆物体，并对克隆物体进行操作','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MDE3Ng==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'2-5'),(7,1,'弹簧球','2016-08-23 14:30:44','2016-08-23 14:30:44',0,0,0,'/happy/Uploads/jiaocairsc/1471934400.sb2','初步了解Scratch的界面，通过这个例子，了解Scatch的几个要素','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA0ODA0OA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'1-1'),(8,1,'气球爆炸','2016-08-23 14:42:18','2016-08-23 14:42:18',2,0,0,'/happy/Uploads/jiaocairsc/1471934638.sb2','该项目进一步强调了克隆的概念，同时给学生启蒙了函数的概念，尤其是计算机中函授的优点和用法。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1ODc0NA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',3,'2-6'),(9,1,'行走的小人','2016-08-23 14:49:37','2016-08-23 14:49:37',0,0,0,'/happy/Uploads/jiaocairsc/1471935011.sb2','行走的小人，完美展现了一个角色不同的状态，最关键的是让学生知道角色的不同状态，好比一个人穿不同的衣服，不论怎么变都是同一个角色，而非出现了不同的角色，提高了学生的逻辑思维能力','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA2Mjc5Ng==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-1'),(10,1,'表情变化','2016-08-23 14:52:07','2016-08-23 14:52:07',0,0,0,'/happy/Uploads/jiaocairsc/1471935238.sb2','这个实例让学生进一步体会随机的概念，特别是glide函数的用法','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA2NDEwOA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-2'),(11,1,'对话','2016-08-23 22:43:16','2016-08-23 22:43:16',0,0,0,'/happy/Uploads/jiaocairsc/1471963498.sb2','在舞台上设计对话，可以引起学生莫大的兴趣，最重要的是，你一言我一语其实体现了时间轴的概念，时序第一次出现在学生的学习环境之中，形象生动的时序可以让学生在玩耍中了解时间轴的含义。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA2NDcxMg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-3'),(12,1,'图形特效','2016-08-23 22:54:58','2016-08-23 22:54:58',0,0,0,'/happy/Uploads/jiaocairsc/1471964172.sb2','图形特效代码比较简单，其实他的目的在于让学生理解不同的特效的具体效果，在以后的编程中可以想起来具体的特效需要从何处寻找。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA4NTAyOA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-4'),(13,1,'图层','2016-08-23 22:57:13','2016-08-23 22:57:13',0,0,0,'/happy/Uploads/jiaocairsc/1471964279.sb2','图层，其实大部分人接触是在photoshop中，图层用通俗的例子讲就是把不同的纸片上下堆叠，从而达到互相遮盖的目的。看似一个平面上的图案其实是不同z轴维度的概念。这个实验可以让学生对3d，z轴有了初步的印象和感觉。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5MDczNg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-5'),(14,1,'击鼓','2016-08-23 22:59:56','2016-08-23 22:59:56',0,0,0,'/happy/Uploads/jiaocairsc/1471964430.sb2','用程序来进行弹奏本身对孩子的吸引力是很大的。不同节奏其实就是时间长短的概念，因此，学生在完成这个实验的同时，其实对时间的把握更为深刻。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5Mjg1Mg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-6'),(15,1,'两只老虎','2016-08-23 23:02:30','2016-08-23 23:02:30',0,0,0,'/happy/Uploads/jiaocairsc/1471964605.sb2','两只老虎是耳熟能详的少儿音乐，节奏很简单。其实在试验中学生只要记住do的发音，从而延推出其他的音符的发音，结合之前的节奏的概念，就可以弹奏这个音乐了。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5MzI2OA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-7'),(16,1,'控制节奏','2016-08-23 23:05:09','2016-08-23 23:05:09',0,0,0,'/happy/Uploads/jiaocairsc/1471964741.sb2','控制节奏是有一次对节奏的把我，画正方形的时候使用比较慢的节奏，后者则加倍了节奏，而小猫留下的印章也是对印章功能的复习。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5NDExMg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-8'),(17,1,'小猫越走越远','2016-08-23 23:06:49','2016-08-23 23:06:49',0,0,0,'/happy/Uploads/jiaocairsc/1471964835.sb2','小猫越走越远其实是对之前节奏和形状变化的综合练习，小猫走远从两个维度来说说声音有节奏的变轻了，小猫身体有节奏的变小上移了。','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5NTI2MA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-9'),(18,1,'烟花','2016-08-23 23:08:42','2016-08-23 23:08:42',0,0,0,'/happy/Uploads/jiaocairsc/1471964954.sb2','绚丽多彩的烟花一下次就吸引了小孩子们的眼睛，其实他是随机的克隆花心，然后通过形变的方式慢慢变大，最后配合有节奏的声音，是一个对之前知识很深的提炼！','<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA5NTkwOA==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>',0,'3-10');

/*Table structure for table `tb_lb` */

DROP TABLE IF EXISTS `tb_lb`;

CREATE TABLE `tb_lb` (
  `lbid` int(2) NOT NULL AUTO_INCREMENT,
  `lbnm` varchar(10) DEFAULT NULL,
  `lbodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`lbid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_lb` */

insert  into `tb_lb`(`lbid`,`lbnm`,`lbodr`) values (1,'基础类',1),(2,'客户类',2);

/*Table structure for table `tb_md` */

DROP TABLE IF EXISTS `tb_md`;

CREATE TABLE `tb_md` (
  `mdid` int(10) NOT NULL AUTO_INCREMENT,
  `f_md_lbid` int(5) DEFAULT NULL,
  `mdmk` varchar(20) DEFAULT NULL,
  `mdnm` varchar(20) DEFAULT NULL,
  `mdodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`mdid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=gbk;

/*Data for the table `tb_md` */

insert  into `tb_md`(`mdid`,`f_md_lbid`,`mdmk`,`mdnm`,`mdodr`) values (1,1,'Md','模块模块',3),(2,1,'Usr','用户模块',4),(3,1,'Rl','角色模块',6),(4,1,'Ath','权限模块',8),(5,1,'Bd','板块模块',9),(6,1,'Atc','文章模块',10),(7,1,'Sys','系统模块',11),(9,1,'Grp','团队模块',5),(11,1,'Usrrl','用户-团队-角色模块',7),(14,1,'Lb','大类别模块',2),(16,2,'Cstm','客户用户模块',1),(17,2,'Cstmgrp','客户团队模块',2),(18,2,'Cstmrl','客户角色',3),(19,2,'Cstmath','客户权限模块',4),(20,2,'Cstmusrcstmgrp','客户用户-客户团队模块',5),(21,2,'Cstmgrpcstmrl','客户团队-客户角色模块',6),(22,2,'Cstmusrcstmrl','客户用户-客户角色模块',7),(23,2,'Cstmcmt','客户评论',8),(25,1,'Aa','aa模块',1),(26,1,'Std','学生',2),(27,1,'Qi','办学班次',2),(28,1,'Work','作品',2),(29,1,'Jiaocai','教材',2);

/*Table structure for table `tb_nd` */

DROP TABLE IF EXISTS `tb_nd`;

CREATE TABLE `tb_nd` (
  `ndid` int(5) NOT NULL AUTO_INCREMENT,
  `ndnm` varchar(10) DEFAULT NULL,
  `ndpid` int(5) DEFAULT NULL,
  `ndodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`ndid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `tb_nd` */

insert  into `tb_nd`(`ndid`,`ndnm`,`ndpid`,`ndodr`) values (1,'拱墅区',2,2),(2,'杭州',3,2),(3,'浙江',0,1),(4,'下沙',5,1),(5,'江干区',2,3),(6,'金华',3,3),(7,'湖州',3,1),(8,'江苏',0,2),(9,'萧山区',2,1),(10,'西湖区',2,4);

/*Table structure for table `tb_qi` */

DROP TABLE IF EXISTS `tb_qi`;

CREATE TABLE `tb_qi` (
  `qiid` int(3) NOT NULL AUTO_INCREMENT,
  `qinm` varchar(20) DEFAULT NULL,
  `qiinttm` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`qiid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tb_qi` */

insert  into `tb_qi`(`qiid`,`qinm`,`qiinttm`) values (1,'第一期','2016-07-13');

/*Table structure for table `tb_rl` */

DROP TABLE IF EXISTS `tb_rl`;

CREATE TABLE `tb_rl` (
  `rlid` int(10) NOT NULL AUTO_INCREMENT,
  `rlnm` varchar(20) DEFAULT NULL,
  `f_rl_grpid` int(5) DEFAULT NULL,
  PRIMARY KEY (`rlid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_rl` */

insert  into `tb_rl`(`rlid`,`rlnm`,`f_rl_grpid`) values (1,'默认角色',1),(2,'视频编辑',3);

/*Table structure for table `tb_sex` */

DROP TABLE IF EXISTS `tb_sex`;

CREATE TABLE `tb_sex` (
  `sexid` int(1) NOT NULL AUTO_INCREMENT,
  `sexnm` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`sexid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_sex` */

insert  into `tb_sex`(`sexid`,`sexnm`) values (1,'男'),(2,'女');

/*Table structure for table `tb_std` */

DROP TABLE IF EXISTS `tb_std`;

CREATE TABLE `tb_std` (
  `stdid` int(5) NOT NULL AUTO_INCREMENT,
  `stdnm` varchar(20) DEFAULT NULL,
  `stdpw` varchar(20) DEFAULT NULL,
  `f_std_sexid` int(3) DEFAULT NULL,
  `stdgrd` varchar(20) DEFAULT NULL,
  `f_std_qiid` int(3) DEFAULT NULL,
  PRIMARY KEY (`stdid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tb_std` */

insert  into `tb_std`(`stdid`,`stdnm`,`stdpw`,`f_std_sexid`,`stdgrd`,`f_std_qiid`) values (1,'李金灏',NULL,1,'小学二年级',1),(2,'张鼎浩',NULL,1,'小学X年级',1),(3,'余欣楠',NULL,2,'小学五年级',1),(4,'刘雨宸',NULL,1,'小学二年级',1),(5,'刑知言',NULL,1,'小学二年级',1),(6,'孙斌测试','9e384',1,'大学四年级',1);

/*Table structure for table `tb_sys` */

DROP TABLE IF EXISTS `tb_sys`;

CREATE TABLE `tb_sys` (
  `sysid` int(2) NOT NULL AUTO_INCREMENT,
  `sysnm` varchar(20) DEFAULT NULL,
  `sysip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sysid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tb_sys` */

insert  into `tb_sys`(`sysid`,`sysnm`,`sysip`) values (1,'xxx','localhost');

/*Table structure for table `tb_tree` */

DROP TABLE IF EXISTS `tb_tree`;

CREATE TABLE `tb_tree` (
  `treeid` int(5) NOT NULL AUTO_INCREMENT,
  `treenm` varchar(10) DEFAULT NULL,
  `treepid` int(5) DEFAULT NULL,
  `treeodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`treeid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `tb_tree` */

insert  into `tb_tree`(`treeid`,`treenm`,`treepid`,`treeodr`) values (1,'拱墅区',2,2),(2,'杭州',3,2),(3,'浙江',0,1),(4,'下沙',5,1),(5,'江干区',2,3),(6,'金华',3,3),(7,'湖州',3,1),(8,'江苏',0,2),(9,'萧山区',2,1),(10,'西湖区',2,4);

/*Table structure for table `tb_usr` */

DROP TABLE IF EXISTS `tb_usr`;

CREATE TABLE `tb_usr` (
  `usrid` int(5) NOT NULL AUTO_INCREMENT,
  `usrmk` tinyint(1) DEFAULT NULL,
  `usrnm` varchar(20) DEFAULT NULL,
  `usrpw` varchar(32) DEFAULT NULL,
  `usrnn` varchar(20) DEFAULT NULL,
  `usrpt` varchar(50) DEFAULT NULL,
  `usraddtm` varchar(20) DEFAULT NULL,
  `usrmdftm` varchar(20) DEFAULT NULL,
  `usrcp` varchar(20) DEFAULT NULL,
  `usrml` varchar(20) DEFAULT NULL,
  `usrps` tinyint(1) DEFAULT NULL,
  `usrvw` tinyint(1) DEFAULT NULL,
  `usrodr` int(5) DEFAULT NULL,
  PRIMARY KEY (`usrid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tb_usr` */

insert  into `tb_usr`(`usrid`,`usrmk`,`usrnm`,`usrpw`,`usrnn`,`usrpt`,`usraddtm`,`usrmdftm`,`usrcp`,`usrml`,`usrps`,`usrvw`,`usrodr`) values (1,1,'admin','1bbd886460827015e5d605ed44252251','超管','/xxx/Public/img/usr/default.jpg','2015-11-26 00:00:00','2016-02-26 03:02:08','13333333333','sunbinovic@163.com',1,1,1),(2,0,'test','1bbd886460827015e5d605ed44252251','测试员','/xxx/Uploads/usr/1456115109.jpg','','2016-02-27 04:02:28','15555555553','',1,1,2);

/*Table structure for table `tb_usrrl` */

DROP TABLE IF EXISTS `tb_usrrl`;

CREATE TABLE `tb_usrrl` (
  `usrrlid` int(5) NOT NULL AUTO_INCREMENT,
  `f_usrrl_usrid` int(8) DEFAULT NULL,
  `f_usrrl_rlid` int(5) DEFAULT NULL,
  PRIMARY KEY (`usrrlid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_usrrl` */

insert  into `tb_usrrl`(`usrrlid`,`f_usrrl_usrid`,`f_usrrl_rlid`) values (3,2,1);

/*Table structure for table `tb_work` */

DROP TABLE IF EXISTS `tb_work`;

CREATE TABLE `tb_work` (
  `workid` int(5) NOT NULL AUTO_INCREMENT,
  `f_work_bdid` int(5) DEFAULT NULL,
  `worktpc` varchar(50) DEFAULT NULL,
  `f_work_stdid` int(5) DEFAULT NULL,
  `workaddtm` varchar(20) DEFAULT NULL,
  `workmdftm` varchar(20) DEFAULT NULL,
  `workcnt` int(5) DEFAULT NULL,
  `workzn` int(5) DEFAULT NULL COMMENT '赞',
  `worktc` int(5) DEFAULT NULL COMMENT '吐槽',
  `workrsc` varchar(100) DEFAULT NULL,
  `worksmr` mediumtext COMMENT '摘要',
  `workmark` int(4) DEFAULT NULL,
  `workvideo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`workid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=gbk;

/*Data for the table `tb_work` */

insert  into `tb_work`(`workid`,`f_work_bdid`,`worktpc`,`f_work_stdid`,`workaddtm`,`workmdftm`,`workcnt`,`workzn`,`worktc`,`workrsc`,`worksmr`,`workmark`,`workvideo`) values (1,1,'弹球',1,'2016-07-21 14:55:14','2016-07-21 14:55:15',4,0,0,'/happy/Uploads/workrsc/1469088018.sb2','锻炼了XXX',90,NULL),(2,1,'正方形画花',6,'2016-07-22 00:06:40','2016-07-22 00:06:40',0,0,0,'/happy/Uploads/workrsc/1469117230.sb2','在书本的基础上加上了自己的见解，很好',98,NULL),(3,1,'小猫追小球',6,'2016-07-22 00:09:33','2016-07-22 00:09:33',0,0,0,'/happy/Uploads/workrsc/1469117383.sb2','学生很好理解了，猫，球，鼠标之间的内在联系，同时初步接触到延时的概念',95,NULL),(4,1,'小猫抢钱袋',6,'2016-07-22 11:00:28','2016-07-22 11:00:28',0,0,0,'/happy/Uploads/workrsc/1469156492.sb2','通过本次实验，孩子初步感受到了虚化的含义。对坐标轴中心点加深了理解，对面向的角度有了一定的认识，对复杂事件中类似的动作有了一定的提取能力，为日后软件分工，以及对未来分工的思维模式有了很好的养成，增加了变量的概念，同时在数学上对步长的理解进一步加深',95,NULL),(5,1,'接苹果游戏',6,'2016-07-22 11:01:58','2016-07-22 11:01:59',0,0,0,'/happy/Uploads/workrsc/1469156561.sb2','进一步加强对坐标轴的理解，首次接触到克隆物体，并对克隆物体进行操作',95,'<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MzUzNg==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>'),(6,1,'火箭打气球',6,'2016-07-22 11:03:03','2016-07-22 11:03:05',3,0,0,'/happy/Uploads/workrsc/1469156617.sb2','进一步加强对坐标轴的理解，首次接触到克隆物体，并对克隆物体进行操作',95,'<embed src=\"http://player.youku.com/player.php/sid/XMTY5MTA1MDE3Ng==/v.swf\" allowFullScreen=\"true\" quality=\"high\" width=\"480\" height=\"400\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\"></embed>');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
